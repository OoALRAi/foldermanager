package util;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FolderManager {
	private File folder;

	public static FolderManager initManager(String pathStr) {
		Path path = Paths.get(pathStr);
		if (Files.exists(path)) {
			return new FolderManager();
		}
		System.out.println("path does not exist!");
		return null;
	}

	public void setFolder(String pathStr) {
		this.folder = new File(pathStr);
	}

	public File getFolder() {
		return folder;
	}
	//TODO filter files in folder and return all ArrayList
	public ArrayList<File> getListOfType(String type) {
		return null;
	}
	//TODO move files of specific type to new Directory.
	public void moveTo(FileTypes type, String path) {
		
	}
		
}
